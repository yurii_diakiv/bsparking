﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryStudio_Parking
{
    class Transaction
    {
        DateTime Time { get; set; }
        int TransportID { get; set; }
        public double Money { get; set; }

        public Transaction(DateTime time, int transportId, double money)
        {
            Time = time;
            TransportID = transportId;
            Money = money;
        }

        public override string ToString()
        {
            return $"Time: {Time}, Transport: {TransportID}, Money: {Money}";
        }
    }
}
