﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BinaryStudio_Parking
{
    class Menu
    {
        private Parking parking;

        public Menu(Parking p)
        {
            parking = p;
        }

        public void AddNewTransport()
        {
            Console.WriteLine("Choose number: ");
            Console.WriteLine("1: add car");
            Console.WriteLine("2: add bus");
            Console.WriteLine("3: add truck");
            Console.WriteLine("4: add motorcycle");
            Console.Write(">>> ");
            string selection = Console.ReadLine();

            double balance = 0;

            do
            {
                Console.Write("Enter balance: ");
                balance = double.Parse(Console.ReadLine());
                if (balance < 0)
                {
                    Console.WriteLine("Enter correct balance!");
                }
            }
            while (balance < 0);

            switch (selection)
            {
                case "1":
                    parking.AddTransport(new Car(balance));
                    break;
                case "2":
                    parking.AddTransport(new Bus(balance));
                    break;
                case "3":
                    parking.AddTransport(new Truck(balance));
                    break;
                case "4":
                    parking.AddTransport(new Motorcycle(balance));
                    break;
                default:
                    Console.WriteLine("Enter correect number!");
                    break;
            }
        }

        public void ReadFile()
        {
            try
            {
                using (StreamReader sr = new StreamReader("../../Transactions.log"))
                {
                    Console.WriteLine(sr.ReadToEnd());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void MenuRun()
        {
            Console.WriteLine("Choose number: ");
            Console.WriteLine("1: current balance of parking");
            Console.WriteLine("2: the amount of money in the last minute");
            Console.WriteLine("3: number of free places");
            Console.WriteLine("4: transactions in the last minute");
            Console.WriteLine("5: history of transactions");
            Console.WriteLine("6: list of transports");
            Console.WriteLine("7: add new transport");
            Console.WriteLine("8: delete transport");
            Console.WriteLine("9: add money to the balance");
            Console.WriteLine("0: exit");

            bool stop = false;
            while (!stop)
            {
                Console.Write(">>> ");
                string selection = Console.ReadLine();
                switch (selection)
                {
                    case "1":
                        Console.Write("Current balance of parking: ");
                        Console.WriteLine(parking.GetBalance());
                        break;
                    case "2":
                        Console.Write("The amount of money in the last minute: ");
                        Console.WriteLine(parking.OneMinuteAmountOfMoney());
                        break;
                    case "3":
                        Console.Write("Number of free places: ");
                        Console.WriteLine(parking.NumberOfFreePlaces() + " / " + Settings.maxParkingCapacity);
                        break;
                    case "4":
                        Console.WriteLine("Transactions in the last minute:");
                        parking.RegisterOutput();
                        break;
                    case "5":
                        Console.WriteLine("History of transactions:");
                        ReadFile();
                        break;
                    case "6":
                        Console.WriteLine("List of transports:");
                        parking.TransportsOutput();
                        break;
                    case "7":
                        AddNewTransport();
                        break;
                    case "8":
                        Console.Write("Enter ID to delete: ");
                        int idToDelete = int.Parse(Console.ReadLine());
                        parking.RemoveTransport(idToDelete);
                        break;
                    case "9":
                        Console.Write("Enter ID to add money: ");
                        int idToAddMoney = int.Parse(Console.ReadLine());

                        double moneyToAdd = 0;

                        do
                        {
                            Console.Write("Enter amount of money: ");
                            moneyToAdd = double.Parse(Console.ReadLine());
                            if (moneyToAdd < 0)
                            {
                                Console.WriteLine("Enter correct amount!");
                            }
                        }
                        while (moneyToAdd < 0);

                        parking.FindTransportToAddMoney(idToAddMoney, moneyToAdd);
                        break;
                    case "0":
                        stop = true;
                        break;
                    default:
                        Console.WriteLine("Enter correct number!");
                        break;
                }
            }
        }
    }
}
