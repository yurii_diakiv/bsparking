﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BinaryStudio_Parking
{
    public class WebClientMenu
    {
        private HttpClient client;

        public WebClientMenu()
        {
           

            client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:5001/");
        }

        public void ShowAllTransports()
        {
            client.DefaultRequestHeaders.Accept.Clear();

            var transportsJson = client.GetStringAsync("api/transport").Result;
            List<Transport> transports = JsonConvert.DeserializeObject<List<Transport>>(transportsJson);

            foreach(var i in transports)
            {
                Console.WriteLine(i);
            }
        }


    }
}
