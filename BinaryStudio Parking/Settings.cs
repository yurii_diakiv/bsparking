﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryStudio_Parking
{
    static class Settings
    {
        public static double balance = 0;
        public static int maxParkingCapacity = 10;
        public static int timeToPay = 5;
        public static double fineCoefficient = 2.5;
        public static Dictionary<string, double> tariffs = new Dictionary<string, double>
        {
            { "Car", 2 },
            { "Truck", 5 },
            { "Bus", 3.5 },
            { "Motorcycle", 1}
        };
    }
}
